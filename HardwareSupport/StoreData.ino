#include <ArduinoJson.h>
#include "FS.h"
class StoreData
{
    JsonObject &json;

  public:
    StoreData()
    {
        loadConfig();
    }
    ~StoreData()
    {
        saveConfig();
    }
    bool loadConfig()
    {
        File configFile = SPIFFS.open("/config.json", "r");
        if (!configFile)
        {
            Serial.println("Failed to open config file");
            return false;
        }

        size_t size = configFile.size();
        if (size > 1024)
        {
            Serial.println("Config file size is too large");
            return false;
        }

        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        // We don't use String here because ArduinoJson library requires the input
        // buffer to be mutable. If you don't use ArduinoJson, you may as well
        // use configFile.readString instead.
        configFile.readBytes(buf.get(), size);

        StaticJsonBuffer<200> jsonBuffer;
        json = jsonBuffer.parseObject(buf.get());

        if (!json.success())
        {
            Serial.println("Failed to parse config file");
            return false;
        }
        return true;
    }
    char *getValue(char *key)
    {
        return json[key];
    }
    char *setValue(char *key, char *value)
    {
        json[key] = value;
    }

    bool saveConfig()
    {
        File configFile = SPIFFS.open("/config.json", "w");
        if (!configFile)
        {
            Serial.println("Failed to open config file for writing");
            return false;
        }

        json.printTo(configFile);
        return true;
    }
}