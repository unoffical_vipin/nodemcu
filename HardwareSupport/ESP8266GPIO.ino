struct IOPin
{
    //static short int const D0 = 16;        // user //Wake
    static short int const D1 = 5;
    static short int const D2 = 4;
    static short int const D3 = 0; //Flash
    //static short int const D4 = 2; //TXD1
    static short int const D5 = 14;  //HSCLK
    static short int const D6 = 12;  //HMISO
    static short int const D7 = 13;  //RXD2 //HMOSI
    static short int const D8 = 15;  //TXD2 //HCS
    /*static short int const D9 = 3;   //RXD0
    static short int const D10 = 1;  //TXD0
    static short int const D11 = 9;  //SDD3
    static short int const D12 = 10; //SDD2*/
};
struct LedPin{
    static short int const LED1_D0 = 16;
    static short int const LED2_D4 = 2;
};